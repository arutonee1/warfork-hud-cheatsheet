# warfork-hud-cheatsheet
This is a cheatsheet of things to know while you're making a HUD for WarFork.

## Documentation (IMPORTANT)
By far, the best place to find documentation for WarFork HUDs is via the [WarSow HUD Scripting page](https://slice.sh/warsow/oldwiki/HUDScripting.html).

## Colors
Color operations use rgba from a decimal from 0 - 1.

## Conversions
To convert from gridspace to pixelspace, multiply by `%VIDHEIGHT / #HEIGHT`.

To convert from pixelspace to gridspace, multiply by `#HEIGHT / %VIDHEIGHT`.

The WIDTH can also be used, depending on the direction you're trying to position things in (HEIGHT for vertical, WIDTH for horizontal).

Note that these should be placed at the end of calculations, otherwise the ratios won't be calculated properly (you can mentally verify this, see [Order of Operations](#Order_of_Operations)).

`%VIDHEIGHT / %VIDWIDTH` can be used to get the "aspect ratio." For some items you should multiply the horizontal component by this to make everything square on all aspect ratios.

## Cursor
The cursor is set on a 800x600 grid. You can reference those with #WIDTH and #HEIGHT.

## Order of Operations
Parentheses don't exist. Also, things are evaluated right to left. This is extremely limiting, but you can make it work.

## Fonts
The setFontSize command uses pixels, which are separate from the grid.
